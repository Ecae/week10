__author__ = 'jhoeflich2017'

codes = { 'A':'H','a':'He','B':'Li','b':'Be','C':'B','c':'C','D':'N','d':'O','E':'F','e':'Ne',
          'F':'Na','f':'Mg','G':'Al','g':'Si','H':'P','h':'S','I':'Cl','i':'Ar','J':'K','j':'Ca',
          'K':'Sc','k':'Ti','L':'V','l':'Cr','M':'Mn','m':'Fe','N':'Co','n':'Ni','O':'Cu','o':'Zn',
          'P':'Ga','p':'Ge','Q':'As','q':'Se','R':'Br','r':'Kr','S':'Rb','s':'Sr','T':'Y','t':'Zr',
          'U':'Nb','u':'Mo','V':'Tc','v':'Ru','W':'Rh','w':'Pd','X':'Ag','x':'Cd','Y':'In','y':'Sn',
          'Z':'Sb','z':'Te','1':'I','2':'Xe','3':'Cs','4':'Ba','5':'La','6':'Ce','7':'Pr','8':'Nd',
          '9':'Pm','0':'Sm',',':'Eu','.':'Gd','!':'Tb','?':'Dy', '|':'|', '(':'(', ')':')', ' ': 'g', '':'', '\n':'\n'}

#decryption codes creation
codes2 = {}
for key, val in codes.items():
        codes2[val] = key

#encryption function
def encryption(code, s):
    word = ""
    for letter in code:
        word += s[letter] + ' '
    return word

#encryption process
with open("unique.txt", 'r') as file:
    fileText = list(file.read())
print(encryption(fileText, codes))
x = (encryption(fileText, codes))

#write encrypt to file
xFile = open('encrypt.txt', 'w')
xFile.write(x)
xFile.close()

#decryption
xList = x.split(' ')
def decryption(codes2,s):
    word = ""
    for letter in codes2:
        word += s[letter]
    return word
print(decryption(xList,codes2))
y = (decryption(xList,codes2))

#write decrpyt to file
yFile = open('decrpyt.txt', 'w')
yFile.write(y)
yFile.close()

